package unsolved;

import java.util.*;

public class LongestCommonSubsequence {
	
	public static void main(String[] args){
		//test
		test();
		//run
		//run();
	}
	
	private static void test(){
		int[] a = {1,2,3,4,1};
		int[] b = {3,4,1,2,1,3};
		findSubsequence(a,b);
	}
	
	private static void run(){
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int m = in.nextInt();
		int[] a = new int[n];
		int[] b = new int[m];
		
		for(int i=0; i<n; i++){	a[i] = in.nextInt(); }
		for(int i=0; i<m; i++){ b[i] = in.nextInt(); }
		
		findSubsequence(a,b);
	}

	private static void findSubsequence(int[] a, int[] b){
		ArrayList<Integer> small = new ArrayList<Integer>();
		ArrayList<Integer> big = new ArrayList<Integer>();
		ArrayList<Integer> temp = new ArrayList<Integer>();
		int biggest = 0;
		
		if(a.length < b.length){ small = aToAL(a);  big = aToAL(b); }
		else				   { small = aToAL(b);  big = aToAL(a); }
		
		int max = small.size();
		
		//big = dropUnsharedMembers(small, big);
		
		if(small.equals(big)){ temp = small;}
		else{
			
		}
		
		
		printSequence(temp);
	}
	
	private static ArrayList<Integer> aToAL(int[] a){
		ArrayList<Integer> result = new ArrayList<Integer>(a.length);
		
		for(int i=0; i<a.length; i++){
			result.set(i, a[i]);
		}
		
		return result;
	}
	
	private static ArrayList<Integer> dropUnsharedMembers(ArrayList<Integer> small, ArrayList<Integer> big){
		ArrayList<Integer> result = new ArrayList<Integer>();
		
		for(int i=0; i< big.size(); i++){
			if( !small.contains( big.get(i) ) ){
				big.remove(i);
			}
		}
		
		result = big;
		
		return result;
	}

	private static void printSequence(ArrayList<Integer> s){
		System.out.print(s.get(0));
		for(int i=1; i<s.size(); i++){
			System.out.print(" " + s.get(i));
		}
		System.out.println();
	}
}
